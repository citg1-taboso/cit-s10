package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	Retrieve all posts
//	localhost:8080/posts
//	@RequestMapping(value="/posts", method = RequestMethod.GET)
	@GetMapping("/posts")
	public String getPosts() {
		return "All posts retrieved";
	}

//	Creating new posts
//	localhost:8080/posts
//	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@PostMapping("/posts")
	public String newPost() {
		return "New Post created";
	}

//	Retrieving a single post
//	localhost:8080/posts/{id}
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid) {
		return "Viewing details of post " + postid;
	}

//	Deleting a post
//	/posts/1234
//	@RequestMapping(value = "", method = RequestMethod.DELETE)
	@DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid) {
		return "The post " + postid + " has been deleted.";
	}

//	Updating post
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
//	automatically imports the proper formatting (JSON).
	@PutMapping("/posts/{postid}")
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post) {
		return post;
	}

//	Retrieving a specific response for a particular user
//	/posts

//	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	@GetMapping("/myPosts")
	public String getMyPost(@RequestHeader(value = "Authorization") String user) {
		return "Posts for " + user + " have been retrieved.";
	}



//	Activity Code (User)
//	Create new user
	@PostMapping("/users")
	public String newUser() {
		return "New user created.";
	}
//  Get all users
	@GetMapping("/users")
	public String getAllUsers() {
		return "All users retrieved.";
	}
//  Get specific user
	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid) {
		return "Viewing details of user " + userid;
	}
//  Delete User
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid, @RequestHeader(value = "Authorization") String user) {
		if(user.isEmpty()) {
			return "Unauthorized access.";
		}
		return "The user " + userid + " has been deleted.";
	}
//  Updating User
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user) {
		return user;
	}


}
